class CreateQuestions < ActiveRecord::Migration[5.0]
  def up
    create_table :questions do |t|
      t.string :name
      t.belongs_to :interview, index: true, foreign_key: true
      t.timestamps
    end
  end

  def down
    drop_table :questions
  end
end
