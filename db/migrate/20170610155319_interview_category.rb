class InterviewCategory < ActiveRecord::Migration[5.0]
  def change
    add_belongs_to :interviews, :category, index: true, foreign_key: true
  end
end
