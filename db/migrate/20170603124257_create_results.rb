class CreateResults < ActiveRecord::Migration[5.0]
  def change
    create_table :results do |t|
      t.belongs_to :session, index: true, foreign_key: true
      t.belongs_to :answer, index: true, foreign_key: true
      t.belongs_to :question, index: true, foreign_key: true
      t.timestamps
    end
  end
end
