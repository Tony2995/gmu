class CreateSessions < ActiveRecord::Migration[5.0]
  def up
    create_table :sessions do |t|
      t.belongs_to :interview, index: true, foreign_key: true
      t.timestamps
    end
  end

  def down
    drop_table :sessions
  end
end
