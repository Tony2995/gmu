class CreateInterviews < ActiveRecord::Migration[5.0]
  def up
    create_table :interviews do |t|
      t.string :name
      t.timestamps
    end
  end

  def down
    drop_table :interviews
  end
end
