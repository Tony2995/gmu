class Result < ApplicationRecord
  belongs_to :session
  belongs_to :question
  belongs_to :answer
end
