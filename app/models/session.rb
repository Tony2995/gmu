class Session < ApplicationRecord
  belongs_to :interview
  has_many :results
end
