class Question < ApplicationRecord
  belongs_to :interview, inverse_of: :questions
  has_many :answers
  has_many :results

  accepts_nested_attributes_for :interview

end
