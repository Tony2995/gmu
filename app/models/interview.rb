class Interview < ApplicationRecord
  has_many :questions, inverse_of: :interview
  has_many :sessions
  belongs_to :category

  accepts_nested_attributes_for :questions

end
