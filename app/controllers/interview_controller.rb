class InterviewController < ApplicationController

  def index
    @interviews = Interview.all
    @session = Session.new
    categories = Category.all

    interviews = [@interviews, categories]

    respond_to do |format|
      format.html
      format.json { render json: interviews}
    end
  end


  def start
    session_test = Session.new(test_params)
    session_test.save

    session[:testing_id] = session_test.id

    redirect_to testing_path

  end


  def show

  end


  private

  def test_params
    params.permit(:interview_id)
  end

end

