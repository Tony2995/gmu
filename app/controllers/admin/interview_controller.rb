class Admin::InterviewController < Admin::AdminController
  before_action :set_interview, only: [:show]

  def index
    @interviews = Interview.all

    respond_to do |format|
      format.html
      format.json { render json: @interviews }
    end
  end

  def new
    @interview = Interview.new
    @question = @interview.questions.build
    categories = Category.all

    respond_to do |format|
      format.html
      format.json { render json: categories }
    end
  end

  def create

    @interview = Interview.new(interview_params)
    @interview.save

    @interview.questions.build

    redirect_to admin_interview_index_path

  end

  def show
    @interview_id = set_interview.id

    questions = @interview.questions
    sessions = @interview.sessions
    # answers = [{ :question => [:answers => []] }]
    answers = []

    @interview.questions.each do |q|
      answer_count = 0
      q.answers.each do |a|
        answer_count = answer_count + a.content.to_i
      end
      answers << {"question" => q.name, "answer" => answer_count}
    end

    @interview = ["questions" => questions, "answers" => answers]

    respond_to do |format|
      format.html
      format.json { render json: @interview }
    end

  end

  private

  def set_interview
    @interview = Interview.find(params[:id]);
  end

  def interview_params
    params.require(:interview).permit(:name, :description, :category_id, questions_attributes: [:name])
  end

end
