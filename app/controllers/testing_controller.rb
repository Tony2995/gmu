class TestingController < ApplicationController
  def index
    test = session[:testing_id]
    session_test = Session.find(test)
    interview_test = Interview.find(session_test.interview_id)
    @questions = interview_test.questions

    respond_to do |format|
      if @questions
        format.html
        format.json { render json: @questions }
      else
        format.json { render json: @questions.erros.full_messsages, status: :unprocessable_entity }
      end
    end
  end

  def save_test
    #  FIXME It's so ugly =(
    data = testing_params[:data]
    data.each do |d|
      answers = Answer.new(d)
      answers.save
      result = Result.new
      result.session_id = session[:testing_id]
      result.question_id = d[:question_id]
      result.answer_id = answers.id
      result.save
    end

    redirect_to action: 'result'
  end

  def result
    #  сколько человек прошли опрос
    # какой процент из них доволен тем-то и тем-то
    # сколько человек ответило по каждлому баллу
    @results = []
    testing = Session.find(session[:testing_id])
    testing.results.each do |r|
      result = {question: r.question.name, answer: r.answer.content}
      @results << result
    end
    
    respond_to do |format|
      if @results
        format.html
        format.json { render json: @results }
      else
        format.json { render json: @results.erros.full_messsages, status: :unprocessable_entity }
      end
    end

  end

  def testing_params
    params.permit(data: [:question_id, :content])
  end
end
