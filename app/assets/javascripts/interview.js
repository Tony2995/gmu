/**
 * Created by tony on 01.06.17.
 */

$(document).ready(function () {
    if ($("#interview").length > 0) {

        let interview = new Vue({
            el: '#interview',
            data: {
                interviews : [],
                categories: []
            },
            methods: {
                medical: function () {

                },
                culture: function () {

                },
                education: function () {

                },
                all: function () {

                }
            },

            mounted() {
                axios.get("/interview.json")
                    .then(function (response) {
                        let interviews = response.data[0];
                        let categories = response.data[1];

                        categories.forEach(function(val) {
                            interview.categories.push(val)
                        });
                        console.log(interviews);
                        interviews.forEach(function (val) {

                            interview.interviews.push({id: val.id, name: val.name, description: val.description})
                        })
                    });
            }
        });
    }
});

