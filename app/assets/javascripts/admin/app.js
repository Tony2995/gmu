/**
 * Created by tony on 01.06.17.
 */

$(document).ready(function () {
    if ($("#app").length > 0) {
        let app = new Vue({
            el: '#app',
            data: {
                name: 'afgsrg',
                questions: [],
                count: 0,
                dinName: '',
                category: '1',
                options: []
            },
            methods: {
                add: function () {
                    this.questions.push({
                        content: '',
                        number: this.count += 1,
                        type: '',
                        dinName: this.dinName + 'interview[questions_attributes][' + this.count + '][name]'

                    })
                },
            },

            mounted() {
                axios.get("/admin/interview/new.json")
                    .then(function (response) {
                        let categories = response.data;

                        categories.forEach(function (val) {
                            app.options.push({text: val.name, value: val.id})
                        })
                    });
            }
        });
    }
});

