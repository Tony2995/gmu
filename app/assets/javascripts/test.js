/**
 * Created by tony on 01.06.17.
 */

$(document).ready(function () {
    if ($("#test").length > 0) {
      let token = $('meta[name=csrf-token]').attr("content");

        let i = 1;

        let test = new Vue({
            el: '#test',

            data: {
                questions_json: [],
                question: {
                    name: '',
                    answer_number: null
                },
                answers: []
            },

            methods: {
                nextQuestion: function () {
                    if (this.questions_json[i]) {
                        let question = this.questions_json[i];

                        this.question.name = question.name;
                        this.answers.push({
                            question_id: question.id,
                            content: this.question.answer_number
                        });
                        this.question.answer_number = null
                    } else {

                        axios.post("/save_test", {
                            data: this.answers
                        })
                        .then(function (response) {
                            console.log(response);
                        })
                        .catch(function (error) {
                            console.log(error);
                        });

                        window.location = "http://localhost:3000/result";
                        return null;

                    }

                    i = i + 1;
                }
            },

            mounted() {
              axios.defaults.headers.common['X-CSRF-Token'] = token;

                axios.get("/testing.json")
                    .then(function (response) {

                        let q = response.data;
                        test.questions_json = q;

                         test.question.name = test.questions_json[0].name.toString();
                         delete test.questions_json[0]

                    })
            }
        });
    }
});
