Rails.application.routes.draw do
  devise_for :users
  resources :interview

  get 'testing', to: 'testing#index'
  post 'save_test', to: 'testing#save_test'

  get 'result', to: 'testing#result'

  post 'start', to: 'interview#start'

  namespace :admin do
    resources :interview
    get '/', to: 'admin#index'
  end

  root to: 'interview#index'
end
